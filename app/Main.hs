-- stack  run -- 10
{-# LANGUAGE RecordWildCards #-}
{-# language OverloadedStrings, QuasiQuotes #-}
module Main where

import Control.Concurrent.STM
import Control.Concurrent
import Control.Monad
import Turtle
import Data.String.Interpolate

data Config = Config
    {   contention :: Int
    }
parseConfig = Config <$> argInt "contention" "contention parameter"

info = [i|
    A list of 'n' TVars each pestered by one threads applying succ to its value.
    A single thread try to read them all together and print the differential of the sum.
    Pass 'n' as argument. With n over 5 you start to see the livelock kicking in. The reader starves.
    Notice that a writer with same requirement (write to all of them atomically) would starve the same.
    The 'n' threads have the smallest requirement and they will not let the fat guy in.
    With n = 100 it's almost impossible to get in.
    If you want to see heap explode, just take away the tick on the modify and guess why you have to kill the main thread after short :-)
    |]

main :: IO ()
main = do
    Config{..}  <- options info parseConfig
    as <- replicateM contention $ newTVarIO (0 :: Int)
    let f n = forkIO $ forever $ atomically $ do
            modifyTVar' (as !! n) succ
    forM_ [0..pred contention]  f 
    let diff b = do
            x <- atomically $ traverse readTVar as
            let b' = sum x
            print (b' - b)
            diff b'
    diff 0
   

